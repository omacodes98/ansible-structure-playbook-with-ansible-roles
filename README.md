# Ansible - Structure Playbooks with Ansible Roles 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

Structur Playbooks with Ansibles Roles 

## Technologies Used 

* Ansble 

* Docker 

* AWS 

* Linux 

* Terraform 

## Steps 

Step 1: Create a copy of one of your playbook 

[copy of playbook](/images/01_created_a_copy_of_one_of_my_playbook.png)

Step 2: Create role folder 

[role folder creation](/images/02_create_roles_folder.png)

Step 3: Create another folder inside the role folder named create_user

[create_user folder created](/images/03_create_another_folder_inside_the_role_folder_named_create_user.png)

Step 4: Create a folder called task inside create_user which is a minimum requirement inside each role folder 

[task folder created](/images/04_create_a_folder_called_task_inside_create_user_which_is_a_minimum_requirement_inside_each_role_folder.png)

Step 5: Inside the task folder create a file called main.yaml 

[main file in create_user](/images/05_inside_the_task_folder_you_have_to_have_a_file_called_main_dot_yaml.png)

Step 6: Create another role directory start_container with the same structure of the above steps 

[start_container role](/images/06_create_another_role_directory_start_container_with_the_same_structure_of_the_above_steps.png)

Step 7: Put necessary logic inside main.yaml, in my case i went into my playbook and cut the create user task and pasted it in main.yaml file in create_user directory 

[cut task](/images/07_i_went_into_my_playbook_and_cut__the_create_user_task.png)
[past task in main](/images/08_pasted_it_in_main_.yaml_file_in_create_user_directory.png)

Step 8: Do the same thing for another task you want a role for, i did the same thing for the copy docker-compose task in playbook and pasted it in main

[past task in main in dir of start_container](/images/09_did_the_same_thing_for_copy_docker_compose_task.png)

Step 9: Now to use roles all that needs to be done is replace tasks with roles and in roles block insert name of role folder you want to use 

[replace tasks in playbook to necessary roles](/images/10_now_to_use_roles_all_we_have_to_do_is_replace_tasks_with_roles_and_in_roles_block_insert_name_of_role_folder_you_want_to_use.png)

Step 10: Use Terraform to create ec2 instance 

[Created ec2 instance using terraform](/images/11_use_terraform_to_create_ec2_instance.png)


Step 11: Test playbook 

    ansible-playbook my-playbook.yaml

[successful playbook](/images/12_successful_playbook.png)

Step 12: In start_container role directory create a directory called files 

[created file directory for start_container](/images/13_in_start_container_role_directory_create_a_directory_called_files.png)

Step 13: Create docker-compose file in file directory 

[docker-compose created](/images/14_create_docker_compose_file_in_that_file_directory.png)

Step 14: Now instead of referencing full path of the file in src you can just reference file name

[file name ref](/images/15_now_instead_of_reference_full_path_of_file_in_src_you_can_just_reference_file_name.png)


Step 15: Test playbook to see if it works fine with that change 

    ansible-playbook my-playbook.yaml

[test playbook 15](/images/16_test_play_success.png)

Step 16: In start container role dir create another direcotry called vars 

[vars directory created](/images/17_in_start_container_role_dir_create_another_directory_called_vars.png)

Step 17: Inside vars dir create file called main.yaml 

[main file created inside vars dir](/images/18_inside_vars_dir_create_file_called_main_dot_yaml.png)

Step 18: Define varaibles like docker registry and username in vars main.yaml file 

[Defining variables](/images/19_define_variables_like_docker_registry_and_username_in_vars_main_dot_yaml_file.png)

Step 19: Reference variables in main.yaml file in container_start role directory

[Referencing variables](/images/20_reference_variables_in_main_dot_tf_file_in_container_start_role_directory.png)

Step 20: Create another folder in start container role named defaults for defaults value 

[defaults file](/images/21_create_another_folder_in_start_container_role_named_defaults_for_defaults_value.png)

Step 21: Create main.yaml file in default directory 

[main file in default dir](/images/22_create_main_dot_yaml_file_in_defaults_dir.png)

Step 22: define default values in main file

[defined values in default main](/images/23_define_default_values_in_main_file.png)

Step 23: Test playbook


[Test playbook final](/images/24_test_playbook_success.png)


## Installation

    brew install ansible 

## Usage 

    ansible-playbook -i host deploy-docker.yaml 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/ansible-structure-playbook-with-ansible-roles.gitt

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/ansible-structure-playbook-with-ansible-roles

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.

## License